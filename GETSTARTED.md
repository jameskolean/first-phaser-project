# Steps to build from scratch

Clone a template with web pack

```javascript
git clone git@github.com:photonstorm/phaser3-project-template.git first-phaser-project
cd first-phaser-project
npm install
npm start
```

Make sure the Production build works

```javascript
npm run build
```

Install the latest Paser

```javascript
npm install phaser@3.24.1
```

Add assets and update index.js

## Deploying

- Create a GitLab project and push the code
- Create a Netlify Project and link to GitLab Project
  - Build Command: npm run build
  - Publish Folder: dist
