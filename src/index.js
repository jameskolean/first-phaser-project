import Phaser from 'phaser'
import background from './assets/images/background.png'
import warrior from './assets/images/warrior.png'
import petDragon from './assets/images/pet_dragon_new.png'
import sword from './assets/images/icon.png'

const config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: 800,
  height: 600,
  scene: {
    init: init,
    preload: preload,
    create: create,
    update: update,
  },
}

const game = new Phaser.Game(config)
function init() {
  console.log('in init')
  this.score = 0
  this.lives = 3
  this.speed = 1.5
  this.dragon_move = 1
  this.score_text
  this.lives_text
  this.end = end
}

function preload() {
  console.log('in preload')
  // lets preload some images that we can use in our game
  this.load.image('background', background)
  this.load.image('player', warrior)
  this.load.image('dragon', petDragon)
  this.load.image('gold', sword)
}
function end() {
  if (this.lives <= 0) {
    this.scene.restart()
  } else {
    this.create()
  }
}

function create() {
  console.log('in create')
  // add the background
  var bg = this.add.sprite(0, 0, 'background')
  bg.setOrigin(0, 0)

  // add score text & game text to screen
  this.scoreText = this.add.text(100, 16, 'score: ' + this.score, {
    fontSize: '32px',
    fill: '#000',
  })
  this.liveText = this.add.text(
    16,
    this.sys.game.config.height - 50,
    'Lives: ' + this.lives,
    { fontSize: '32px', fill: '#000' }
  )

  // add player
  this.player = this.add.sprite(100, 150, 'player')
  this.player.setScale(0.3)

  // add monster
  this.dragon = this.add.sprite(350, 150, 'dragon')
  this.dragon.setScale(0.1)

  // add gold
  this.gold = this.add.sprite(650, 150, 'gold')
  this.gold.setScale(0.5)
  console.log('lalala', this.input)
  this.keyRight = this.input.keyboard.addKey('Right')
  this.keyLeft = this.input.keyboard.addKey('Left')
  this.keyUp = this.input.keyboard.addKey('Up')
  this.keyDown = this.input.keyboard.addKey('Down')
}

function update() {
  // Move player
  if (this.keyRight.isDown) {
    this.player.x += this.speed
  } else if (this.keyLeft.isDown) {
    this.player.x -= this.speed
  }
  if (this.keyUp.isDown) {
    this.player.y -= this.speed
  } else if (this.keyDown.isDown) {
    this.player.y += this.speed
  }
  this.player.x = Math.max(50, this.player.x)
  this.player.y = Math.max(75, this.player.y)
  this.player.x = Math.min(this.sys.game.config.width - 50, this.player.x)
  this.player.y = Math.min(this.sys.game.config.height - 75, this.player.y)

  if (
    Phaser.Geom.Intersects.RectangleToRectangle(
      this.player.getBounds(),
      this.dragon.getBounds()
    )
  ) {
    this.lives--
    this.liveText.setText('Lives: ' + this.lives)
    this.end()
  }

  if (
    Phaser.Geom.Intersects.RectangleToRectangle(
      this.player.getBounds(),
      this.gold.getBounds()
    )
  ) {
    this.score += 50
    this.scoreText.setText('Score: ' + this.score)
    this.end()
  }

  if (this.dragon.y >= 500) {
    // Go up
    this.dragon_move = -1
  } else if (this.dragon.y <= 100) {
    // Go down
    this.dragon_move = 1
  }

  this.dragon.y += this.dragon_move
}
